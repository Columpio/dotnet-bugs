﻿open System

let f x =
    let y = x * x

    y * y + x


[<EntryPoint>]
let main _ =
    let r = new Random()
    printfn "%A" (f <| r.Next())

    0 // return an integer exit code
